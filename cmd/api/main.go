package main

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	"github.com/spf13/afero"
	flag "github.com/spf13/pflag"
	"github.com/spf13/viper"
	conf "gitlab.com/AswinKakarot/golang-testapp-pokemon"
	"gitlab.com/AswinKakarot/golang-testapp-pokemon/pkg/config"
	modv1 "gitlab.com/AswinKakarot/golang-testapp-pokemon/pkg/models/v1"
	"gitlab.com/AswinKakarot/golang-testapp-pokemon/pkg/routers"
)

type ServerConf struct {
	Port         int
	ReadTimeout  int
	WriteTimeout int
	IdleTimeout  int
}

func NewServerConf() *ServerConf {
	return &ServerConf{}
}

type AuthConf struct {
	Secret   string
	Duration time.Duration
}

func NewAuthConf() *AuthConf {
	return &AuthConf{}
}

func main() {
	// Command line flags
	confFile := flag.String("config",
		"app",
		"Config Path")
	logLvl := flag.String(
		"log.level",
		"info",
		"Logging level (can be one of debug, info, error)")
	flag.Parse()

	// Initialize Logger
	config.SetupZaprLogger(*logLvl)
	log := config.GetLogger()
	config.PrintInfo(log)
	// Loading configuration file
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	viper.AutomaticEnv()
	viper.SetFs(afero.NewReadOnlyFs(afero.FromIOFS{FS: conf.FS}))
	viper.SetConfigFile(*confFile)

	if err := viper.ReadInConfig(); err != nil {
		log.Error(err, "error reading configuration file")
		os.Exit(1)
	}

	// Initialize Database
	dbConf := config.NewDatabaseConfig()
	if err := viper.UnmarshalKey("database", dbConf); err != nil {
		log.Error(err, "error getting database config")
	}
	if err := config.InitDatabase(dbConf); err != nil {
		log.Error(err, "Unable to start the database connection with the error")
	}
	// Initialize Schemas
	modv1.InitializeModeles(config.GetDbConnection())

	// App server config
	srvConf := NewServerConf()
	if err := viper.UnmarshalKey("server", srvConf); err != nil {
		log.Error(err, "error getting database config")
	}

	// Auth config
	authConf := NewAuthConf()
	if err := viper.UnmarshalKey("auth", authConf); err != nil {
		log.Error(err, "error getting jwt auth configuration")
	}
	r := routers.NewRouter(log, authConf.Secret, authConf.Duration)
	r.Init()
	log.Info(fmt.Sprintf("Application Starting on Port %d", srvConf.Port))
	s := http.Server{
		Addr:         fmt.Sprintf(":%d", srvConf.Port),
		Handler:      r.Router,
		ReadTimeout:  time.Duration(srvConf.ReadTimeout) * time.Second,
		WriteTimeout: time.Duration(srvConf.WriteTimeout) * time.Second,
		IdleTimeout:  time.Duration(srvConf.IdleTimeout) * time.Second,
	}
	errs := make(chan error, 1)

	go func() {
		if err := s.ListenAndServe(); err != nil {
			errs <- err
		}
	}()

	go func() {
		c := make(chan os.Signal, 1)
		signal.Notify(c, syscall.SIGINT)
		errs <- fmt.Errorf("%s", <-c)
	}()

	log.Error(<-errs, "Shutting down application with")

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	s.Shutdown(ctx)
}
