package routers

import (
	"fmt"
	"net/http"
	"time"

	"github.com/go-logr/logr"
	"github.com/gorilla/mux"
	ctrlv1 "gitlab.com/AswinKakarot/golang-testapp-pokemon/pkg/controllers/v1"
)

type Jwt struct {
	Secret   string
	Duration time.Duration
}

type Router struct {
	Router *mux.Router
	Logger *logr.Logger
	Jwt
}

func NewRouter(log *logr.Logger, s string, d time.Duration) *Router {
	return &Router{
		Router: mux.NewRouter(),
		Logger: log,
		Jwt: Jwt{
			Secret:   s,
			Duration: d,
		},
	}
}

func (r *Router) Init() {
	r.Router.HandleFunc("/ping", func(rw http.ResponseWriter, r *http.Request) {
		rw.WriteHeader(http.StatusOK)
		fmt.Fprintf(rw, "pong")
	})

	cuHandler := ctrlv1.NewCleanUp()
	r.Router.HandleFunc("/cleanup", cuHandler.Apply).Methods("POST")

	r.CreateTypeRoutes()
	r.CreateItemRoutes()
	r.CreateMoveRoutes()
	r.CreateUserRoutes()
}
