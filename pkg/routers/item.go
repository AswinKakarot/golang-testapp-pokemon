package routers

import (
	"os"

	ctrlv1 "gitlab.com/AswinKakarot/golang-testapp-pokemon/pkg/controllers/v1"
	"gitlab.com/AswinKakarot/golang-testapp-pokemon/pkg/middlewares"
)

func (r *Router) CreateItemRoutes() {
	var signingKey = []byte(os.Getenv("JWT_SECRET_KEY"))
	ma := middlewares.NewAuth(signingKey)
	ic := ctrlv1.NewItemCategory(r.Logger)
	r.Router.HandleFunc("/item-category", ma.IsAuthorized(ic.Write)).Methods("POST")
	r.Router.HandleFunc("/item-category", ma.IsAuthorized(ic.ReadAll)).Methods("GET")
	r.Router.HandleFunc("/item-category/{name}", ma.IsAuthorized(ic.Read)).Methods("GET")
	r.Router.HandleFunc("/item-category/{name}", ma.IsAuthorized(ic.Delete)).Methods("DELETE")

	a := ctrlv1.NewAttribs(r.Logger)
	r.Router.HandleFunc("/item-attribs", ma.IsAuthorized(a.Write)).Methods("POST")
	r.Router.HandleFunc("/item-attribs", ma.IsAuthorized(a.ReadAll)).Methods("GET")
	r.Router.HandleFunc("/item-attribs/{name}", ma.IsAuthorized(a.Read)).Methods("GET")
	r.Router.HandleFunc("/item-attribs/{name}", ma.IsAuthorized(a.Delete)).Methods("DELETE")

	i := ctrlv1.NewItem(r.Logger)
	r.Router.HandleFunc("/item", ma.IsAuthorized(i.Write)).Methods("POST")
	r.Router.HandleFunc("/item", ma.IsAuthorized(i.ReadAll)).Methods("GET")
	r.Router.HandleFunc("/item/{id}", ma.IsAuthorized(i.Read)).Methods("GET")
	r.Router.HandleFunc("/item/{id}", ma.IsAuthorized(i.Delete)).Methods("DELETE")
}
