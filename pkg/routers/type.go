package routers

import (
	ctrlv1 "gitlab.com/AswinKakarot/golang-testapp-pokemon/pkg/controllers/v1"
	"gitlab.com/AswinKakarot/golang-testapp-pokemon/pkg/middlewares"
)

func (r *Router) CreateTypeRoutes() {
	ma := middlewares.NewAuth([]byte(r.Secret))
	typeHandler := ctrlv1.NewType(r.Logger)
	r.Router.HandleFunc("/type", ma.IsAuthorized(typeHandler.Write)).Methods("POST")
	r.Router.HandleFunc("/type", ma.IsAuthorized(typeHandler.ReadAll)).Methods("GET")
	r.Router.HandleFunc("/type/{id}", ma.IsAuthorized(typeHandler.Read)).Methods("GET")
	r.Router.HandleFunc("/type/{id}", ma.IsAuthorized(typeHandler.Delete)).Methods("DELETE")
}
