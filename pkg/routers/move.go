package routers

import (
	"os"

	ctrlv1 "gitlab.com/AswinKakarot/golang-testapp-pokemon/pkg/controllers/v1"
	"gitlab.com/AswinKakarot/golang-testapp-pokemon/pkg/middlewares"
)

func (r *Router) CreateMoveRoutes() {
	var signingKey = []byte(os.Getenv("JWT_SECRET_KEY"))
	ma := middlewares.NewAuth(signingKey)
	dc := ctrlv1.NewDamageClass(r.Logger)
	r.Router.HandleFunc("/damage-class", ma.IsAuthorized(dc.Write)).Methods("POST")
	r.Router.HandleFunc("/damage-class/{id}", ma.IsAuthorized(dc.Read)).Methods("GET")
	r.Router.HandleFunc("/damage-class", ma.IsAuthorized(dc.ReadAll)).Methods("GET")
	r.Router.HandleFunc("/damage-class/{id}", ma.IsAuthorized(dc.Delete)).Methods("DELETE")

	mt := ctrlv1.NewMoveTarget(r.Logger)
	r.Router.HandleFunc("/move-target", ma.IsAuthorized(mt.Write)).Methods("POST")
	r.Router.HandleFunc("/move-target/{id}", ma.IsAuthorized(mt.Read)).Methods("GET")
	r.Router.HandleFunc("/move-target", ma.IsAuthorized(mt.ReadAll)).Methods("GET")
	r.Router.HandleFunc("/move-target/{id}", ma.IsAuthorized(mt.Delete)).Methods("DELETE")

	m := ctrlv1.NewMove(r.Logger)
	r.Router.HandleFunc("/move", ma.IsAuthorized(m.Write)).Methods("POST")
	r.Router.HandleFunc("/move/{id}", ma.IsAuthorized(m.Read)).Methods("GET")
	r.Router.HandleFunc("/move", ma.IsAuthorized(m.ReadAll)).Methods("GET")
	r.Router.HandleFunc("/move/{id}", ma.IsAuthorized(m.Delete)).Methods("DELETE")
}
