package routers

import (
	ctrlv1 "gitlab.com/AswinKakarot/golang-testapp-pokemon/pkg/controllers/v1"
)

func (r *Router) CreateUserRoutes() {
	su := ctrlv1.NewUser(r.Secret, r.Duration)
	r.Router.HandleFunc("/signup", su.SignUp).Methods("POST")
	r.Router.HandleFunc("/login", su.Login).Methods("POST")
	c := ctrlv1.NewCountry()
	r.Router.HandleFunc("/country", c.Write).Methods("POST")
}
