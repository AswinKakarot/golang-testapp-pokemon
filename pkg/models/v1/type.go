package v1

import (
	"gorm.io/gorm"
)

// Pokemon Type
type Type struct {
	ID   string `gorm:"type:uuid;primary_key;default:uuid_generate_v4()"`
	Name string `json:"name"`
}

func NewType() *Type {
	return &Type{}
}

func (t *Type) Create(db *gorm.DB) error {
	r := db.Where("name = ?", t.Name).Find(t)
	if r.RowsAffected == 0 {
		return db.Create(t).Error
	}
	return db.Model(t).Updates(t).Error
}

func (t *Type) Select(db *gorm.DB, id string) error {
	return db.First(t, "id = ?", id).Error
}

func SelectTypes(db *gorm.DB, lim, off int) (*[]Type, error) {
	t := make([]Type, lim)
	if err := db.Offset(off).Limit(lim).Find(&t).Error; err != nil {
		return nil, err
	}
	return &t, nil
}

func (t *Type) Delete(db *gorm.DB) error {
	return db.Delete(t).Error
}

type MoveType struct {
	Type
	MoveID string `gorm:"type:uuid" json:"-"`
}
