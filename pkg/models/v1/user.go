package v1

import (
	"errors"
	"fmt"
	"regexp"
	"strings"
	"time"

	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

// Date is a custom date type for Date of Birth
type Date time.Time

// Custom Marshaler for Date Type
func (d Date) MarshalJSON() ([]byte, error) {
	t := time.Time(d)
	stamp := fmt.Sprintf("\"%s\"", t.Format("01/01/2006"))
	return []byte(stamp), nil
}

// Custom Unmarshaller for Date Type
func (d *Date) UnmarshalJSON(b []byte) (err error) {
	s := strings.Trim(string(b), `"`)
	o, err := time.Parse("01/01/2006", s)
	*d = Date(o)
	return
}

// Country is custom country type with the country code
type Country struct {
	Name        string `gorm:"primaryKey" json:"name"`
	CountryCode string `json:"code"`
	PhoneCode   string `json:"phone_code"`
}

func NewCountry() *Country {
	return &Country{}
}

func (c *Country) Create(db *gorm.DB) error {
	return db.Create(c).Error
}

func SelectCountry(db *gorm.DB) ([]Country, error) {
	c := make([]Country, 198)
	q := db.Find(c)
	if q.Error != nil {
		return nil, q.Error
	}
	return c, nil
}

func (c *Country) Delete(db *gorm.DB) error {
	return db.Delete(c).Error
}

type UserCountry struct {
	ID string `gorm:"type:uuid;primaryKey;default:uuid_generate_v4()" json:"-"`
	Country
	UserID string `json:"-"`
}

func NewUserCountry() *UserCountry {
	return &UserCountry{}
}

type Email string

func (e Email) MarshalText() ([]byte, error) {
	return []byte(e), nil
}

func (e *Email) UnmarshalText(b []byte) error {
	s := string(b)
	re := regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")
	if !re.MatchString(s) {
		return errors.New("invalid email")
	}
	*e = Email(s)
	return nil
}

type User struct {
	ID          string      `gorm:"type:uuid;primaryKey;default:uuid_generate_v4()"`
	Username    string      `json:"username"`
	Firstname   string      `json:"first_name"`
	Lastname    string      `json:"last_name"`
	DOB         Date        `json:"dob"`
	Country     UserCountry `json:"country"`
	Email       Email       `json:"email"`
	PhoneNumber string      `json:"phone_number"`
}

func (u *User) BeforeCreate(db *gorm.DB) error {
	c := NewCountry()
	q := db.Model(&Country{}).Find(c, "name = ?", u.Country.Name)
	if q.Error != nil {
		return q.Error
	}
	if q.RowsAffected == 0 {
		return errors.New("country not found")
	}
	return nil
}

func (u *User) Create(db *gorm.DB) error {
	r := db.Model(&User{}).
		Find(u, "username = ?", u.Username)
	if r.Error != nil {
		return r.Error
	}
	if r.RowsAffected == 0 {
		return db.Model(&User{}).
			Session(&gorm.Session{FullSaveAssociations: true}).
			Create(u).Error
	}
	return errors.New("user exists")
}

// func (u *User) BeforeUpdate(db *gorm.DB) error {
// 	c := NewUserCountry()

// }

func (u *User) Update(db *gorm.DB) error {
	return db.Model(&User{}).
		Where("username = ?", u.Username).
		Omit(clause.Associations).
		Updates(u).Error
}

func (u *User) Select(db *gorm.DB) error {
	return db.Model(&User{}).
		Find(u, "username = ?", u.Username).
		Error
}

func (u *User) Delete(db *gorm.DB) error {
	return db.Model(&User{}).
		Delete(u).Error
}
