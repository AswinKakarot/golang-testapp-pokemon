package v1

import (
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type MoveMetadata struct {
	ID            string       `gorm:"type:uuid;primaryKey;default:uuid_generate_v4()"`
	Ailment       MoveAilment  `json:"ailment"`
	Category      MoveCategory `gorm:"foreignKey:MoveMetadataID;constraint:OnUpdate:CASCADE,OnDelete:SET NULL;" json:"category"`
	MinHits       int          `json:"minHits"`
	MaxHits       int          `json:"maxHits"`
	MinTurns      int          `json:"minTurns"`
	MaxTurns      int          `json:"maxTurns"`
	Drain         int          `json:"drain"`
	Healing       int          `json:"healing"`
	CritRate      int          `json:"critRate"`
	AilmentChance int          `json:"ailmentChance"`
	FlinchChance  int          `json:"flinchChance"`
	StatChance    int          `json:"statChance"`
	MoveID        string       `json:"-"`
}
type MoveAilment struct {
	ID             string `gorm:"type:uuid;primaryKey;default:uuid_generate_v4()" json:"-"`
	Name           string `json:"name"`
	MoveMetadataID string `json:"-"`
}

type StatChange struct {
	ID     string `gorm:"type:uuid;primaryKey;default:uuid_generate_v4()" json:"-"`
	Change int    `json:"change"`
	Stat   Stat   `gorm:"foreignKey:MoveStatsID" json:"stat"`
	MoveID string `json:"-"`
}

type MoveCategory struct {
	ID             string `gorm:"type:uuid;primaryKey;default:uuid_generate_v4()" json:"-"`
	Name           string `json:"name"`
	Description    string `json:"description"`
	MoveMetadataID string `json:"-"`
}

// DamageClass is a NamedAPI
type DamageClass struct {
	ID          string `gorm:"type:uuid;primaryKey;default:uuid_generate_v4()" json:"id"`
	Name        string `json:"name"`
	Description string `json:"description"`
	MoveID      string `json:"-"`
}

func NewDamageClass() *DamageClass {
	return &DamageClass{}
}

// Write DamageClass Instance to database
func (dc *DamageClass) Create(db *gorm.DB) error {
	r := db.Model(&DamageClass{}).
		Find(dc, "name = ?", dc.Name)
	if r.Error != nil {
		return r.Error
	}
	if r.RowsAffected == 0 {
		if err := db.Create(dc).Error; err != nil {
			return err
		}
	} else {
		if err := db.Session(&gorm.Session{
			FullSaveAssociations: true,
		}).Updates(dc).Error; err != nil {
			return err
		}
	}
	return db.Session(&gorm.Session{FullSaveAssociations: true}).Save(dc).Error
}

// Get one DamageClass instance from the database based on the name parameter
func (dc *DamageClass) Select(db *gorm.DB, id string) error {
	return db.Model(&DamageClass{}).
		Preload(clause.Associations).
		Find(dc, "id = ?", id).Error
}

// Get a batch of DamageClass instances from the database
func SelectDamageClass(db *gorm.DB, lim, off int) (*[]DamageClass, error) {
	dc := make([]DamageClass, lim)
	if err := db.Model(&DamageClass{}).
		Preload(clause.Associations).
		Find(&dc).Error; err != nil {
		return nil, err
	}
	return &dc, nil
}

// Delete an instance of DamageClass
func (dc *DamageClass) Delete(db *gorm.DB) error {
	return db.Select(clause.Associations).Delete(dc).Error
}

// MoveTarget is a NamedAPI
type MoveTarget struct {
	ID          string `gorm:"type:uuid;primaryKey;default:uuid_generate_v4()" json:"id"`
	Name        string `json:"name"`
	Description string `json:"description"`
	MoveID      string `json:"-"`
}

func NewMoveTarget() *MoveTarget {
	return &MoveTarget{}
}

func (mt *MoveTarget) Create(db *gorm.DB) error {
	r := db.Model(&MoveTarget{}).
		Find(mt, "name = ?", mt.Name)
	if r.Error != nil {
		return r.Error
	}
	if r.RowsAffected == 0 {
		return db.Model(&MoveTarget{}).
			Create(mt).Error
	}
	return db.Model(&MoveTarget{}).
		Where("id = ?", mt.ID).
		Updates(mt).Error
}

func (mt *MoveTarget) Select(db *gorm.DB, id string) error {
	return db.Model(&MoveTarget{}).
		Preload(clause.Associations).
		Find("id = ?", id).Error
}

func SelectMoveTarget(db *gorm.DB, lim, off int) (*[]MoveTarget, error) {
	mt := make([]MoveTarget, lim)
	o := db.Model(&MoveTarget{}).
		Limit(lim).
		Offset(off).
		Preload(clause.Associations).
		Find(&mt)
	if o.Error != nil {
		return nil, o.Error
	}
	return &mt, nil
}

func (mt *MoveTarget) Delete(db *gorm.DB) error {
	return db.Select(clause.Associations).Delete(mt).Error
}

// Move is a NamedAPI
type Move struct {
	ID          string       `gorm:"type:uuid;primaryKey;default:uuid_generate_v4()" json:"id"`
	Name        string       `json:"name"`
	Accuracy    int          `json:"accuracy"`
	Pp          int          `json:"pp"`
	Power       int          `json:"power"`
	DamageClass DamageClass  `json:"damageClass"`
	Metadata    MoveMetadata `gorm:"constraint:OnUpdate:CASCADE,OnDelete:SET NULL;" json:"metadata"`
	StatChanges []StatChange `gorm:"constraint:OnUpdate:CASCADE,OnDelete:SET NULL;" json:"statChanges"`
	Target      MoveTarget   `json:"moveTarget"`
	Type        MoveType     `json:"type"`
}

func NewMove() *Move {
	return &Move{}
}

func (m *Move) BeforeUpdate(db *gorm.DB) (err error) {
	if err = db.Model(&MoveMetadata{}).
		Where("move_id = ?", m.ID).
		Updates(m.Metadata).Error; err != nil {
		return
	}
	if err = db.Model(&DamageClass{}).
		Where("move_id = ?", m.ID).
		Updates(m.DamageClass).Error; err != nil {
		return
	}
	if err = db.Model(&MoveTarget{}).
		Where("move_id = ?", m.ID).
		Updates(m.Target).Error; err != nil {
		return
	}
	if err = db.Model(&MoveType{}).
		Where("move_id = ?", m.ID).
		Updates(m.Type).Error; err != nil {
		return
	}
	return
}

func (m *Move) Create(db *gorm.DB) error {
	r := db.Model(&Move{}).Find(m, "name = ?", m.Name)
	if r.Error != nil {
		return r.Error
	}
	if r.RowsAffected == 0 {
		if err := db.Create(m).Error; err != nil {
			return err
		}
		return db.Session(&gorm.Session{
			FullSaveAssociations: true}).
			Save(m).Error
	} else {
		return db.Omit(clause.Associations).Updates(m).Error
	}
}

func (m *Move) Select(db *gorm.DB, id string) error {
	return db.Model(&Move{}).
		Preload(clause.Associations).
		Find(m, "id = ?", id).Error
}

func SelectMove(db *gorm.DB, lim, off int) (*[]Move, error) {
	m := make([]Move, lim)
	o := db.Model(&Move{}).
		Limit(lim).
		Offset(off).
		Preload(clause.Associations).
		Find(&m)
	if o.Error != nil {
		return nil, o.Error
	}
	return &m, nil
}

func (m *Move) Delete(db *gorm.DB) error {
	return db.Select(clause.Associations).Delete(m).Error
}
