package v1

import "gorm.io/gorm"

func InitializeModeles(db *gorm.DB) {
	db.Exec(`CREATE EXTENSION IF NOT EXISTS "uuid-ossp"`)
	db.AutoMigrate(&Type{},
		&MoveType{},
		&Item{},
		&ItemCategory{},
		&Attribs{},
		&ItemAttribs{},
		&MoveMetadata{},
		&MoveAilment{},
		&StatChange{},
		&MoveCategory{},
		&DamageClass{},
		&MoveTarget{},
		&Move{},
		&User{},
		&Auth{},
		&UserCountry{},
		&Country{},
	)
}

func CleanUpModels(db *gorm.DB) {
	db.Migrator().DropTable(&Type{},
		&MoveType{},
		&Item{},
		&ItemCategory{},
		&Attribs{},
		&ItemAttribs{},
		&MoveMetadata{},
		&MoveAilment{},
		&StatChange{},
		&MoveCategory{},
		&DamageClass{},
		&MoveTarget{},
		&Move{},
		&User{},
		&Auth{},
		&UserCountry{},
		&Country{},
	)
	InitializeModeles(db)
}
