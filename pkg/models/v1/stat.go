package v1

// Stat is a NamedAPI
type Stat struct {
	ID           string      `gorm:"type:uuid;primaryKey;default:uuid_generate_v4()" json:"id"`
	Name         string      `json:"name"`
	IsBattleOnly bool        `json:"isBattleOnly"`
	MoveStatsID  string      `gorm:"type:uuid"`
	DamageClass  DamageClass `gorm:"type:uuid;foreignKey:StatsID"`
}
