package v1

import (
	"errors"
	"time"

	"github.com/dgrijalva/jwt-go"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

type Jwt struct {
	Secret   string        `json:"-"`
	Duration time.Duration `json:"duration"`
}
type Auth struct {
	Username string `gorm:"primaryKey" json:"username"`
	Password string `json:"password"`
	Email    Email  `json:"email"`
	Role     string `json:"-" gorm:"default:user"`
}

func NewAuth() *Auth {
	return &Auth{}
}

func (a *Auth) Create(db *gorm.DB) error {
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(a.Password), 8)
	if err != nil {
		return err
	}
	a.Password = string(hashedPassword)
	return db.Model(&Auth{}).
		Create(a).Error
}

func (a *Auth) Delete(db *gorm.DB) error {
	return db.Model(&Auth{}).
		Delete(a).Error
}

type Token struct {
	Role        string `json:"role"`
	Email       Email  `json:"email"`
	TokenString string `json:"token"`
	Valid       bool   `json:"valid"`
	Jwt
}

func NewToken() *Token {
	return &Token{}
}

func (t *Token) Generate() (err error) {
	// TODO: move this to config file
	var signingKey = []byte(t.Secret)
	token := jwt.New(jwt.SigningMethodHS256)
	claims := token.Claims.(jwt.MapClaims)
	claims["authorized"] = true
	claims["email"] = t.Email
	claims["role"] = t.Role
	claims["exp"] = time.Now().Add(time.Minute * t.Duration).Unix()
	if t.TokenString, err = token.SignedString(signingKey); err != nil {
		// t.Logger.Info("Unable to create a signing key with error: %s", err)
		return
	}
	return
}

func (a *Auth) Authenticate(db *gorm.DB, s string, d time.Duration) (*Token, error) {
	o := &Auth{}
	if a.Username != "" {
		q := db.Model(&Auth{}).
			Find(o, "username = ?", a.Username)
		if q.Error != nil {
			return nil, q.Error
		}
	} else if a.Email != "" {
		q := db.Model(&Auth{}).
			Find(o, "email = ?", a.Username)
		if q.Error != nil {
			return nil, q.Error
		}
	} else {
		return nil, errors.New("invalid username/email")
	}
	if err := bcrypt.CompareHashAndPassword([]byte(o.Password), []byte(a.Password)); err != nil {
		return nil, err
	}
	t := NewToken()
	t.Jwt = Jwt{
		Secret:   s,
		Duration: d,
	}
	t.Email = o.Email
	t.Role = o.Role
	if err := t.Generate(); err != nil {
		return nil, err
	}
	return t, nil
}
