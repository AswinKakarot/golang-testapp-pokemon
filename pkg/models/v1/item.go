package v1

import (
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type ItemCategory struct {
	Name        string `gorm:"primaryKey" json:"name"`
	Description string `json:"description,omitempty"`
}

func NewItemCategory() *ItemCategory {
	return &ItemCategory{}
}

func (it *ItemCategory) Create(db *gorm.DB) error {
	r := db.Model(&ItemCategory{}).
		Where("name = ?", it.Name).
		Find(it)
	if r.Error != nil {
		return r.Error
	}
	if r.RowsAffected == 0 {
		return db.Model(&ItemCategory{}).
			Create(it).Error
	}
	return db.Session(&gorm.Session{
		FullSaveAssociations: true,
	}).Model(&ItemCategory{}).
		Updates(it).Error
}

func (it *ItemCategory) Select(db *gorm.DB, name string) error {
	return db.First(it, "name = ?", name).Error
}

func SelectItemCategories(db *gorm.DB, lim, off int) (*[]ItemCategory, error) {
	it := make([]ItemCategory, lim)
	if err := db.Offset(off).
		Limit(lim).
		Find(&it).Error; err != nil {
		return nil, err
	}
	return &it, nil
}

func (it *ItemCategory) Delete(db *gorm.DB) error {
	return db.Delete(it).Error
}

type Attribs struct {
	Name        string `gorm:"primaryKey" json:"name"`
	Description string `json:"description"`
}

func NewAttribs() *Attribs {
	return &Attribs{}
}

func (it *Attribs) Create(db *gorm.DB) error {
	r := db.Model(&Attribs{}).
		Where("name = ?", it.Name).
		Find(it)
	if r.Error != nil {
		return r.Error
	}
	if r.RowsAffected == 0 {
		return db.Create(it).Error
	}
	return db.Updates(it).Error
}

func (it *Attribs) Select(db *gorm.DB, name string) error {
	return db.First(it, "name = ?", name).Error
}

func SelectAttribs(db *gorm.DB, lim, off int) (*[]Attribs, error) {
	it := make([]Attribs, lim)
	if err := db.Offset(off).
		Limit(lim).
		Find(&it).Error; err != nil {
		return nil, err
	}
	return &it, nil
}

func (it *Attribs) Delete(db *gorm.DB) error {
	return db.Delete(it).Error
}

type ItemAttribs struct {
	Attribs
	ItemID string `gorm:"type:uuid" json:"-"`
}

func NewItemAttrib() *ItemAttribs {
	return &ItemAttribs{}
}

type Item struct {
	ID           string        `gorm:"type:uuid;primaryKey;default:uuid_generate_v4()" json:"id"`
	Name         string        `json:"name"`
	Cost         int           `json:"cost"`
	CategoryName string        `json:"categoryName"`
	Category     ItemCategory  `gorm:"foreignKey:CategoryName;references:Name" json:"category"`
	Attributes   []ItemAttribs `gorm:"constraint:OnUpdate:CASCADE,OnDelete:SET NULL;" json:"attributes"`
}

func NewItem() *Item {
	return &Item{}
}

func (it *Item) BeforeCreate(db *gorm.DB) error {
	var il = []ItemAttribs{}
	for _, e := range it.Attributes {
		tmp := NewAttribs()
		ilt := *NewItemAttrib()
		at := db.Model(&Attribs{}).
			Find(tmp, "name = ?", e.Name)
		if err := at.Error; err != nil {
			return err
		}
		if at.RowsAffected != 0 {
			ilt.Name = tmp.Name
			ilt.Description = tmp.Description
			il = append(il, ilt)
		}
	}
	it.Attributes = il
	return nil
}

func (it *Item) BeforeUpdate(db *gorm.DB) error {
	var il = []ItemAttribs{}
	for _, e := range it.Attributes {
		tmp := NewAttribs()
		ilt := *NewItemAttrib()
		at := db.Model(&Attribs{}).Find(tmp, "name = ?", e.Name)
		if err := at.Error; err != nil {
			return err
		}
		if at.RowsAffected != 0 {
			ilt.Name = tmp.Name
			ilt.Description = tmp.Description
			il = append(il, ilt)
		}
	}
	it.Attributes = il
	return nil
}

func (it *Item) Create(db *gorm.DB) error {
	r := db.Model(&Item{}).Find(it, "name = ?", it.Name)
	if r.Error != nil {
		return r.Error
	}
	if r.RowsAffected == 0 {
		if err := db.Model(&Item{}).
			Create(it).Error; err != nil {
			return err
		}
		return db.Session(&gorm.Session{
			FullSaveAssociations: true,
		}).Save(it).Error
	}
	if err := db.Model(&Item{}).
		Where("name = ?", it.Name).
		Updates(it).Error; err != nil {
		return err
	}
	return db.Session(&gorm.Session{
		FullSaveAssociations: true,
	}).Save(it).Error
}

func (it *Item) Select(db *gorm.DB, id string) error {
	return db.Model(&Item{}).
		Preload(clause.Associations).
		Find(it, "id = ?", id).Error
}

func SelectItems(db *gorm.DB, lim, off int) (*[]Item, error) {
	it := make([]Item, lim)
	if err := db.Model(&Item{}).
		Preload(clause.Associations).
		Find(&it).Error; err != nil {
		return nil, err
	}
	return &it, nil
}

func (it *Item) Delete(db *gorm.DB) error {
	return db.Select(clause.Associations).
		Delete(it).Error
}
