package v1

import (
	"fmt"
	"net/http"

	"github.com/go-logr/logr"
	"github.com/gorilla/mux"
	"gitlab.com/AswinKakarot/golang-testapp-pokemon/pkg/config"
	modv1 "gitlab.com/AswinKakarot/golang-testapp-pokemon/pkg/models/v1"
	"gitlab.com/AswinKakarot/golang-testapp-pokemon/pkg/utils"
)

type ItemCategory struct {
	Logger *logr.Logger
}

func NewItemCategory(log *logr.Logger) *ItemCategory {
	return &ItemCategory{
		Logger: log,
	}
}

func (i *ItemCategory) Write(rw http.ResponseWriter, r *http.Request) {
	if r.Header.Get("Role") != "admin" {
		http.Error(rw, "unauthorized", http.StatusUnauthorized)
		return
	}
	it := modv1.NewItemCategory()
	db := config.GetDbConnection()
	if err := utils.DecodeJson(r.Body, it); err != nil {
		i.Logger.Info("failed to decode Item Category request", err)
		http.Error(rw, "failed to decode", http.StatusBadRequest)
		return
	}
	if err := it.Create(db); err != nil {
		i.Logger.Info("failed to write to item_categories table", err)
		http.Error(rw, "failed to write", http.StatusInternalServerError)
		return
	}
	rw.WriteHeader(http.StatusOK)
	utils.EncodeJson(rw, it)
}

func (i *ItemCategory) Read(rw http.ResponseWriter, r *http.Request) {
	if r.Header.Get("Role") != "admin" && r.Header.Get("Role") != "user" {
		http.Error(rw, "unauthorized", http.StatusUnauthorized)
		return
	}
	name := mux.Vars(r)["name"]
	it := modv1.NewItemCategory()
	db := config.GetDbConnection()
	if err := it.Select(db, name); err != nil {
		i.Logger.Info("failed to read from item_categories table", err)
		http.Error(rw, "failed to read", http.StatusInternalServerError)
		return
	}
	rw.WriteHeader(http.StatusOK)
	utils.EncodeJson(rw, it)
}

func (i *ItemCategory) ReadAll(rw http.ResponseWriter, r *http.Request) {
	if r.Header.Get("Role") != "admin" && r.Header.Get("Role") != "user" {
		http.Error(rw, "unauthorized", http.StatusUnauthorized)
		return
	}
	db := config.GetDbConnection()
	o, err := modv1.SelectItemCategories(db, 10, 0)
	if err != nil {
		i.Logger.Info("failed to read from item_categories table", err)
		http.Error(rw, "failed to read", http.StatusInternalServerError)
		return
	}
	rw.WriteHeader(http.StatusOK)
	utils.EncodeJson(rw, o)
}

func (i *ItemCategory) Delete(rw http.ResponseWriter, r *http.Request) {
	if r.Header.Get("Role") != "admin" {
		http.Error(rw, "unauthorized", http.StatusUnauthorized)
		return
	}
	it := modv1.NewItemCategory()
	db := config.GetDbConnection()
	it.Name = mux.Vars(r)["name"]
	if err := it.Delete(db); err != nil {
		i.Logger.Info("failed to delete from item_categories table", err)
		http.Error(rw, "not found", http.StatusBadRequest)
		return
	}
	rw.WriteHeader(http.StatusOK)
	fmt.Fprintf(rw, "successfully deleted")
}

// Attrib Handle Functions
type Attrib struct {
	Logger *logr.Logger
}

func NewAttribs(log *logr.Logger) *Attrib {
	return &Attrib{
		Logger: log,
	}
}

func (a *Attrib) Write(rw http.ResponseWriter, r *http.Request) {
	if r.Header.Get("Role") != "admin" {
		http.Error(rw, "unauthorized", http.StatusUnauthorized)
		return
	}
	it := modv1.NewAttribs()
	db := config.GetDbConnection()
	if err := utils.DecodeJson(r.Body, it); err != nil {
		a.Logger.Info("failed to decode Attrib json", err)
		http.Error(rw, "failed to decode", http.StatusBadRequest)
		return
	}
	if err := it.Create(db); err != nil {
		a.Logger.Info("failed to write to attribs table", err)
		http.Error(rw, "failed to write", http.StatusInternalServerError)
		return
	}
	rw.WriteHeader(http.StatusOK)
	utils.EncodeJson(rw, it)
}

func (a *Attrib) Read(rw http.ResponseWriter, r *http.Request) {
	if r.Header.Get("Role") != "admin" && r.Header.Get("Role") != "user" {
		http.Error(rw, "unauthorized", http.StatusUnauthorized)
		return
	}
	name := mux.Vars(r)["name"]
	it := modv1.NewAttribs()
	db := config.GetDbConnection()
	if err := it.Select(db, name); err != nil {
		a.Logger.Info("failed to read from attribs table", err)
		http.Error(rw, "failed to read", http.StatusInternalServerError)
		return
	}
	rw.WriteHeader(http.StatusOK)
	utils.EncodeJson(rw, it)
}

func (a *Attrib) ReadAll(rw http.ResponseWriter, r *http.Request) {
	if r.Header.Get("Role") != "admin" && r.Header.Get("Role") != "user" {
		http.Error(rw, "unauthorized", http.StatusUnauthorized)
		return
	}
	db := config.GetDbConnection()
	o, err := modv1.SelectAttribs(db, 10, 0)
	if err != nil {
		a.Logger.Info("failed to read from attribs table", err)
		http.Error(rw, "failed to read", http.StatusInternalServerError)
		return
	}
	rw.WriteHeader(http.StatusOK)
	utils.EncodeJson(rw, o)
}

func (a *Attrib) Delete(rw http.ResponseWriter, r *http.Request) {
	if r.Header.Get("Role") != "admin" {
		http.Error(rw, "unauthorized", http.StatusUnauthorized)
		return
	}
	it := modv1.NewAttribs()
	db := config.GetDbConnection()
	it.Name = mux.Vars(r)["name"]
	if err := it.Delete(db); err != nil {
		a.Logger.Info("failed to delete from attribs table", err)
		http.Error(rw, "not found", http.StatusBadRequest)
		return
	}
	rw.WriteHeader(http.StatusOK)
	fmt.Fprintf(rw, "successfully deleted")
}

// Item Handle Functions
type Item struct {
	Logger *logr.Logger
}

func NewItem(log *logr.Logger) *Item {
	return &Item{
		Logger: log,
	}
}

func (i *Item) Write(rw http.ResponseWriter, r *http.Request) {
	if r.Header.Get("Role") != "admin" {
		http.Error(rw, "unauthorized", http.StatusUnauthorized)
		return
	}
	it := modv1.NewItem()
	db := config.GetDbConnection()
	if err := utils.DecodeJson(r.Body, it); err != nil {
		i.Logger.Info("failed to decode item request", err)
		http.Error(rw, "failed to decode", http.StatusBadRequest)
		return
	}
	if err := it.Create(db); err != nil {
		i.Logger.Info("failed to write to items table", err)
		http.Error(rw, "failed to write", http.StatusInternalServerError)
		return
	}
	rw.WriteHeader(http.StatusOK)
	utils.EncodeJson(rw, it)
}

func (i *Item) Read(rw http.ResponseWriter, r *http.Request) {
	if r.Header.Get("Role") != "admin" && r.Header.Get("Role") != "user" {
		http.Error(rw, "unauthorized", http.StatusUnauthorized)
		return
	}
	id := mux.Vars(r)["id"]
	it := modv1.NewItem()
	db := config.GetDbConnection()
	if err := it.Select(db, id); err != nil {
		i.Logger.Info("failed to read from items table", err)
		http.Error(rw, "failed to read", http.StatusInternalServerError)
		return
	}
	rw.WriteHeader(http.StatusOK)
	utils.EncodeJson(rw, it)
}

func (i *Item) ReadAll(rw http.ResponseWriter, r *http.Request) {
	if r.Header.Get("Role") != "admin" && r.Header.Get("Role") != "user" {
		http.Error(rw, "unauthorized", http.StatusUnauthorized)
		return
	}
	db := config.GetDbConnection()
	o, err := modv1.SelectItems(db, 10, 0)
	if err != nil {
		i.Logger.Info("failed to read from items table", err)
		http.Error(rw, "failed to read", http.StatusInternalServerError)
		return
	}
	rw.WriteHeader(http.StatusOK)
	utils.EncodeJson(rw, o)
}

func (i *Item) Delete(rw http.ResponseWriter, r *http.Request) {
	if r.Header.Get("Role") != "admin" {
		http.Error(rw, "unauthorized", http.StatusUnauthorized)
		return
	}
	it := modv1.NewItem()
	db := config.GetDbConnection()
	it.ID = mux.Vars(r)["id"]
	if err := it.Delete(db); err != nil {
		i.Logger.Info("failed to delete from items table", err)
		http.Error(rw, "not found", http.StatusBadRequest)
		return
	}
	rw.WriteHeader(http.StatusOK)
	fmt.Fprintf(rw, "successfully deleted")
}
