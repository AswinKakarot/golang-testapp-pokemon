package v1

import (
	"fmt"
	"net/http"

	"github.com/go-logr/logr"
	"github.com/gorilla/mux"
	"gitlab.com/AswinKakarot/golang-testapp-pokemon/pkg/config"
	modv1 "gitlab.com/AswinKakarot/golang-testapp-pokemon/pkg/models/v1"
	"gitlab.com/AswinKakarot/golang-testapp-pokemon/pkg/utils"
)

type DamageClass struct {
	Logger *logr.Logger
}

func NewDamageClass(log *logr.Logger) *DamageClass {
	return &DamageClass{
		Logger: log,
	}
}

func (d *DamageClass) Write(rw http.ResponseWriter, r *http.Request) {
	if r.Header.Get("Role") != "admin" {
		http.Error(rw, "unauthorized", http.StatusUnauthorized)
		return
	}
	dc := modv1.NewDamageClass()
	db := config.GetDbConnection()
	if err := utils.DecodeJson(r.Body, dc); err != nil {
		d.Logger.Info("failed to decode damage_classes request", err)
		http.Error(rw, "failed to decode", http.StatusBadRequest)
		return
	}
	if err := dc.Create(db); err != nil {
		d.Logger.Info("failed to write to damage_classes table", err)
		http.Error(rw, "failed to write", http.StatusInternalServerError)
		return
	}
	rw.WriteHeader(http.StatusOK)
	utils.EncodeJson(rw, dc)

}

func (d *DamageClass) Read(rw http.ResponseWriter, r *http.Request) {
	if r.Header.Get("Role") != "admin" && r.Header.Get("Role") != "user" {
		http.Error(rw, "unauthorized", http.StatusUnauthorized)
		return
	}
	id := mux.Vars(r)["id"]
	dc := modv1.NewDamageClass()
	db := config.GetDbConnection()
	if err := dc.Select(db, id); err != nil {
		d.Logger.Info("failed to read from damage_classes table", err)
		http.Error(rw, "failed to read", http.StatusInternalServerError)
		return
	}
	rw.WriteHeader(http.StatusOK)
	utils.EncodeJson(rw, dc)
}

func (d *DamageClass) ReadAll(rw http.ResponseWriter, r *http.Request) {
	if r.Header.Get("Role") != "admin" && r.Header.Get("Role") != "user" {
		http.Error(rw, "unauthorized", http.StatusUnauthorized)
		return
	}
	db := config.GetDbConnection()
	o, err := modv1.SelectDamageClass(db, 10, 0)
	if err != nil {
		d.Logger.Info("failed to read from damage_classes table", err)
		http.Error(rw, "failed to read", http.StatusInternalServerError)
		return
	}
	rw.WriteHeader(http.StatusOK)
	utils.EncodeJson(rw, o)
}

func (d *DamageClass) Delete(rw http.ResponseWriter, r *http.Request) {
	if r.Header.Get("Role") != "admin" {
		http.Error(rw, "unauthorized", http.StatusUnauthorized)
		return
	}
	dc := modv1.NewDamageClass()
	db := config.GetDbConnection()
	dc.ID = mux.Vars(r)["id"]
	if err := dc.Delete(db); err != nil {
		d.Logger.Info("failed to delete from damage_classes table")
		http.Error(rw, "not found", http.StatusBadRequest)
		return
	}
	rw.WriteHeader(http.StatusOK)
	fmt.Fprintf(rw, "successfully deleted")
}

type MoveTarget struct {
	Logger *logr.Logger
}

func NewMoveTarget(log *logr.Logger) *MoveTarget {
	return &MoveTarget{
		Logger: log,
	}
}

func (m *MoveTarget) Write(rw http.ResponseWriter, r *http.Request) {
	if r.Header.Get("Role") != "admin" {
		http.Error(rw, "unauthorized", http.StatusUnauthorized)
		return
	}
	db := config.GetDbConnection()
	mt := modv1.NewMoveTarget()
	if err := utils.DecodeJson(r.Body, mt); err != nil {
		m.Logger.Info("failed to decode move_target request", err)
		http.Error(rw, "failed to decode", http.StatusBadRequest)
		return
	}
	if err := mt.Create(db); err != nil {
		m.Logger.Info("failed to write to move_targets table")
		http.Error(rw, "failed to write", http.StatusInternalServerError)
		return
	}
	rw.WriteHeader(http.StatusOK)
	utils.EncodeJson(rw, mt)
}

func (m *MoveTarget) Read(rw http.ResponseWriter, r *http.Request) {
	if r.Header.Get("Role") != "admin" && r.Header.Get("Role") != "user" {
		http.Error(rw, "unauthorized", http.StatusUnauthorized)
		return
	}
	db := config.GetDbConnection()
	mt := modv1.NewMoveTarget()
	if err := mt.Select(db, mux.Vars(r)["id"]); err != nil {
		m.Logger.Info("failed to read from move_targets table", err)
		http.Error(rw, "failed to read", http.StatusInternalServerError)
		return
	}
	rw.WriteHeader(http.StatusOK)
	utils.EncodeJson(rw, mt)
}

func (m *MoveTarget) ReadAll(rw http.ResponseWriter, r *http.Request) {
	if r.Header.Get("Role") != "admin" && r.Header.Get("Role") != "user" {
		http.Error(rw, "unauthorized", http.StatusUnauthorized)
		return
	}
	db := config.GetDbConnection()
	o, err := modv1.SelectMoveTarget(db, 10, 0)
	if err != nil {
		m.Logger.Info("failed to read from move_targets table", err)
		http.Error(rw, "failed to read", http.StatusInternalServerError)
	}
	rw.WriteHeader(http.StatusOK)
	utils.EncodeJson(rw, o)
}

func (m *MoveTarget) Delete(rw http.ResponseWriter, r *http.Request) {
	if r.Header.Get("Role") != "admin" {
		http.Error(rw, "unauthorized", http.StatusUnauthorized)
		return
	}
	db := config.GetDbConnection()
	mt := modv1.NewMoveTarget()
	mt.ID = mux.Vars(r)["id"]
	if err := mt.Delete(db); err != nil {
		m.Logger.Info("failed to delete from move_targets table", err)
		http.Error(rw, "not found", http.StatusBadRequest)
		return
	}
	rw.WriteHeader(http.StatusOK)
	fmt.Fprintf(rw, "successfully deleted")
}

type Move struct {
	Logger *logr.Logger
}

func NewMove(log *logr.Logger) *Move {
	return &Move{
		Logger: log,
	}
}

func (m *Move) Write(rw http.ResponseWriter, r *http.Request) {
	if r.Header.Get("Role") != "admin" {
		http.Error(rw, "unauthorized", http.StatusUnauthorized)
		return
	}
	db := config.GetDbConnection()
	mv := modv1.NewMove()
	if err := utils.DecodeJson(r.Body, mv); err != nil {
		m.Logger.Info("failed to decode move request", err)
		http.Error(rw, "failed to decode", http.StatusBadRequest)
		return
	}
	if err := mv.Create(db); err != nil {
		m.Logger.Info("failed to write to moves table", err)
		http.Error(rw, "failed to write", http.StatusInternalServerError)
		return
	}
	rw.WriteHeader(http.StatusOK)
	utils.EncodeJson(rw, mv)
}

func (m *Move) Read(rw http.ResponseWriter, r *http.Request) {
	if r.Header.Get("Role") != "admin" && r.Header.Get("Role") != "user" {
		http.Error(rw, "unauthorized", http.StatusUnauthorized)
		return
	}
	db := config.GetDbConnection()
	mv := modv1.NewMove()
	id := mux.Vars(r)["id"]
	if err := mv.Select(db, id); err != nil {
		m.Logger.Info("failed to read from moves table", err)
		http.Error(rw, "failed to read", http.StatusInternalServerError)
		return
	}
	rw.WriteHeader(http.StatusOK)
	utils.EncodeJson(rw, mv)
}

func (m *Move) ReadAll(rw http.ResponseWriter, r *http.Request) {
	if r.Header.Get("Role") != "admin" && r.Header.Get("Role") != "user" {
		http.Error(rw, "unauthorized", http.StatusUnauthorized)
		return
	}
	db := config.GetDbConnection()
	o, err := modv1.SelectMove(db, 10, 0)
	if err != nil {
		m.Logger.Info("failed to read from moves table", err)
		http.Error(rw, "failed to read", http.StatusInternalServerError)
		return
	}
	rw.WriteHeader(http.StatusOK)
	utils.EncodeJson(rw, o)
}

func (m *Move) Delete(rw http.ResponseWriter, r *http.Request) {
	if r.Header.Get("Role") != "admin" {
		http.Error(rw, "unauthorized", http.StatusUnauthorized)
		return
	}
	db := config.GetDbConnection()
	mv := modv1.NewMove()
	mv.ID = mux.Vars(r)["id"]
	if err := mv.Delete(db); err != nil {
		m.Logger.Info("failed to delete from moves table", err)
		http.Error(rw, "not found", http.StatusBadRequest)
		return
	}
	rw.WriteHeader(http.StatusOK)
	fmt.Fprintln(rw, "successfully deleted")
}
