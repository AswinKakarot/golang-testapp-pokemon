package v1

import (
	"fmt"
	"net/http"

	"github.com/go-logr/logr"
	"github.com/gorilla/mux"
	"gitlab.com/AswinKakarot/golang-testapp-pokemon/pkg/config"
	modv1 "gitlab.com/AswinKakarot/golang-testapp-pokemon/pkg/models/v1"
	"gitlab.com/AswinKakarot/golang-testapp-pokemon/pkg/utils"
)

// Pokemon Type
type Type struct {
	Logger *logr.Logger
}

func NewType(log *logr.Logger) *Type {
	return &Type{
		Logger: log,
	}
}

func (t *Type) Write(rw http.ResponseWriter, r *http.Request) {
	if r.Header.Get("Role") != "admin" {
		http.Error(rw, "unauthorized", http.StatusUnauthorized)
		return
	}
	ty := modv1.NewType()
	db := config.GetDbConnection()
	if err := utils.DecodeJson(r.Body, ty); err != nil {
		t.Logger.Info("failed to decode type request", err)
		http.Error(rw, "failed to decode", http.StatusBadRequest)
		return
	}
	if err := ty.Create(db); err != nil {
		t.Logger.Info("failed to write to types table", err)
		http.Error(rw, "failed to write", http.StatusInternalServerError)
		return
	}
	rw.WriteHeader(http.StatusOK)
	utils.EncodeJson(rw, ty)
}

func (t *Type) Read(rw http.ResponseWriter, r *http.Request) {
	if r.Header.Get("Role") != "admin" && r.Header.Get("Role") != "user" {
		http.Error(rw, "unauthorized", http.StatusUnauthorized)
		return
	}
	id := mux.Vars(r)["id"]
	ty := modv1.NewType()
	db := config.GetDbConnection()
	if err := ty.Select(db, id); err != nil {
		t.Logger.Info("failed to read from types table", err)
		http.Error(rw, "failed to read", http.StatusInternalServerError)
		return
	}
	rw.WriteHeader(http.StatusOK)
	utils.EncodeJson(rw, ty)
}

func (t *Type) ReadAll(rw http.ResponseWriter, r *http.Request) {
	if r.Header.Get("Role") != "admin" && r.Header.Get("Role") != "user" {
		http.Error(rw, "unauthorized", http.StatusUnauthorized)
		return
	}
	db := config.GetDbConnection()
	o, err := modv1.SelectTypes(db, 10, 0)
	if err != nil {
		t.Logger.Info("failed to read from types table", err)
		http.Error(rw, "failed to read", http.StatusInternalServerError)
		return
	}
	rw.WriteHeader(http.StatusOK)
	utils.EncodeJson(rw, o)
}

func (t *Type) Delete(rw http.ResponseWriter, r *http.Request) {
	if r.Header.Get("Role") != "admin" {
		http.Error(rw, "unauthorized", http.StatusUnauthorized)
		return
	}
	db := config.GetDbConnection()
	ty := modv1.NewType()
	ty.ID = mux.Vars(r)["id"]
	if err := ty.Delete(db); err != nil {
		t.Logger.Info("failed to delete from types table", err)
		http.Error(rw, "not found", http.StatusBadRequest)
		return
	}
	rw.WriteHeader(http.StatusOK)
	fmt.Fprintf(rw, "successfully deleted")
}
