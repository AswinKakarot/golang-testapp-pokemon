package v1

import (
	"net/http"

	"gitlab.com/AswinKakarot/golang-testapp-pokemon/pkg/config"
	modv1 "gitlab.com/AswinKakarot/golang-testapp-pokemon/pkg/models/v1"
)

type CleanUp struct{}

func NewCleanUp() *CleanUp {
	return &CleanUp{}
}

func (cu *CleanUp) Apply(rw http.ResponseWriter, r *http.Request) {
	if r.Header.Get("Role") != "admin" {
		http.Error(rw, "unauthorized", http.StatusUnauthorized)
		return
	}
	db := config.GetDbConnection()
	modv1.CleanUpModels(db)
	rw.WriteHeader(http.StatusOK)
}
