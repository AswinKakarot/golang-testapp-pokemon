package v1

import (
	"fmt"
	"net/http"
	"time"

	"github.com/go-logr/logr"
	"gitlab.com/AswinKakarot/golang-testapp-pokemon/pkg/config"
	modv1 "gitlab.com/AswinKakarot/golang-testapp-pokemon/pkg/models/v1"
	"gitlab.com/AswinKakarot/golang-testapp-pokemon/pkg/utils"
)

type SignUp struct {
	modv1.User
	Password string `json:"password"`
	Role     string `json:"role"`
}

func NewSignUp() *SignUp {
	return &SignUp{}
}

type Jwt struct {
	Secret   string
	Duration time.Duration
}

type User struct {
	Logger *logr.Logger
	Jwt
}

func NewUser(s string, d time.Duration) *User {
	return &User{
		Jwt: Jwt{
			Secret:   s,
			Duration: d,
		},
	}
}

func (u *User) SignUp(rw http.ResponseWriter, r *http.Request) {
	db := config.GetDbConnection()
	s := NewSignUp()
	if err := utils.DecodeJson(r.Body, s); err != nil {
		u.Logger.Info("%s", err)
		http.Error(rw, "failed to decode", http.StatusInternalServerError)
		return
	}
	// Create Authentication Entry
	a := modv1.NewAuth()
	a.Username = s.User.Username
	a.Password = s.Password
	a.Email = s.User.Email
	a.Role = s.Role
	// Create User entry
	us := s.User
	if err := us.Create(db); err != nil {
		if err.Error() == "user exists" {
			http.Error(rw, "user exists", http.StatusBadRequest)
			return
		}
		if err.Error() == "country not found" {
			http.Error(rw, "country not found", http.StatusBadRequest)
			return
		}
		u.Logger.Info("user creation failed", err)
		http.Error(rw, "user creation failed", http.StatusInternalServerError)
		return
	}
	if err := a.Create(db); err != nil {
		u.Logger.Info("user creation failed", err)
		http.Error(rw, "user creation failed", http.StatusInternalServerError)
		return
	}
	rw.WriteHeader(http.StatusOK)
	fmt.Fprintf(rw, us.ID)
}

func (u *User) Login(rw http.ResponseWriter, r *http.Request) {
	db := config.GetDbConnection()
	a := modv1.NewAuth()
	if err := utils.DecodeJson(r.Body, a); err != nil {
		http.Error(rw, "unauthorized", http.StatusUnauthorized)
		return
	}
	token, err := a.Authenticate(db, u.Jwt.Secret, u.Jwt.Duration)
	if err != nil {
		http.Error(rw, "unauthorized", http.StatusUnauthorized)
		return
	}
	rw.WriteHeader(http.StatusOK)
	fmt.Fprintf(rw, "%s", token.TokenString)
}

type Country struct {
}

func NewCountry() *Country {
	return &Country{}
}

func (c *Country) Write(rw http.ResponseWriter, r *http.Request) {
	db := config.GetDbConnection()
	co := modv1.NewCountry()
	utils.DecodeJson(r.Body, co)
	if err := co.Create(db); err != nil {
		http.Error(rw, "country creation failed", http.StatusInternalServerError)
		return
	}
	rw.WriteHeader(http.StatusOK)
	fmt.Fprintf(rw, "country created successfully")
}
