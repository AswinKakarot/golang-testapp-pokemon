package middlewares

import (
	"errors"
	"log"
	"net/http"
	"strings"

	"github.com/dgrijalva/jwt-go"
)

type Auth struct {
	Secret []byte
}

func NewAuth(s []byte) *Auth {
	return &Auth{
		Secret: s,
	}
}

func (a *Auth) IsAuthorized(next http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		authHeader := strings.Split(r.Header.Get("Authorization"), "Bearer ")
		if len(authHeader) != 2 {
			log.Printf("Malformed token")
			http.Error(rw, "malformed token", http.StatusUnauthorized)
			return
		}
		jwtToken := authHeader[1]
		token, err := jwt.Parse(jwtToken, func(token *jwt.Token) (interface{}, error) {
			if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, errors.New("failed to parse token")
			}
			return a.Secret, nil
		})
		if err != nil {
			log.Printf("%s", err)
			http.Error(rw, "token expired", http.StatusUnauthorized)
			return
		}
		if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
			if claims["role"] == "admin" {
				r.Header.Set("Role", "admin")
				next.ServeHTTP(rw, r)
				return
			} else if claims["role"] == "user" {
				r.Header.Set("Role", "user")
				next.ServeHTTP(rw, r)
				return
			}
		}
		http.Error(rw, "unauthorized", http.StatusUnauthorized)
	})
}
