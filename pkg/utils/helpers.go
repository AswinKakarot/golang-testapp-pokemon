package utils

import (
	"encoding/json"
	"io"
)

func DecodeJson(r io.Reader, x interface{}) error {
	d := json.NewDecoder(r)
	return d.Decode(x)
}

func EncodeJson(w io.Writer, x interface{}) error {
	e := json.NewEncoder(w)
	return e.Encode(x)
}
