package config

import (
	"fmt"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type DatabaseConfig struct {
	Host     string
	Port     int
	Username string
	Password string
	Database string
}

func NewDatabaseConfig() *DatabaseConfig {
	return &DatabaseConfig{}
}

var (
	Database *gorm.DB
)

func InitDatabase(cfg *DatabaseConfig) error {
	var err error
	dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%d",
		cfg.Host,
		cfg.Username,
		cfg.Password,
		cfg.Database,
		cfg.Port)
	Database, err = gorm.Open(postgres.Open(dsn), &gorm.Config{})
	return err
}

func GetDbConnection() *gorm.DB {
	return Database
}
