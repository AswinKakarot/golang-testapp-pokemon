package config

import (
	"fmt"
	"log"
	"runtime"

	"github.com/go-logr/logr"
	"github.com/go-logr/zapr"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

var Logger logr.Logger

func SetupZaprLogger(level string) {
	var (
		err    error
		logger *zap.Logger
		config zap.Config
	)
	config = zap.NewDevelopmentConfig()
	var zaplevel zapcore.Level
	if err := zaplevel.UnmarshalText([]byte(level)); err != nil {
		log.Fatal(err)
	}
	config.Level.SetLevel(zaplevel)
	skipZaprCaller := zap.AddCallerSkip(0)
	logger, err = config.Build(skipZaprCaller)
	if err != nil {
		log.Fatalf("unable to initialize logger: %v", err)
	}
	Logger = zapr.NewLogger(logger)
}

func GetLogger() *logr.Logger {
	return &Logger
}

func PrintInfo(log *logr.Logger) {
	log.Info(fmt.Sprintf("Go Version: %s", runtime.Version()))
	log.Info(fmt.Sprintf("Go OS/Arch: %s/%s", runtime.GOOS, runtime.GOARCH))
	log.Info(fmt.Sprintf("GOMAXPROCS: %d", runtime.GOMAXPROCS(-1)))
}
